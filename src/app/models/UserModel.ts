export class UserModel {
    constructor(public fName: string,
                public lName: string,
                public age: number
    ){}
}
import { Component, ElementRef, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import { THROW_IF_NOT_FOUND } from '@angular/core/src/di/injector';

export class User { 
  fName: string;
  lName: string;
  age: number;
} 

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent  {

  title = 'test-project';
  data: User[] = [];
  dataTable: any;
  dtOptions: DataTables.Settings;
  constructor(private eL: ElementRef, private chRef: ChangeDetectorRef){}
   
  ngOnInit(){
    this.dataTable = $('#tableData').DataTable()
  }

  ngAfterViewInit() {
    let self = this;

     let domElem = this.eL.nativeElement.querySelector('#test').addEventListener('change',
                    function(evt) {
                        var file = evt.target.files[0];
                        var reader = new FileReader();
                        reader.onload = (function(thefile){
                          return function(e){
                            let x = JSON.parse(e.target.result)
                            let nData = []
                            for (let i = 0; i < x.length; i++) {
                              let item = x[i];
                              nData.push([item.fName,item.lName,item.age]); 
                            }
                            self.dataTable.rows.add(nData).draw();
                          }
                        })(file);
                        reader.readAsText(file)
    }, false);
  }


}
